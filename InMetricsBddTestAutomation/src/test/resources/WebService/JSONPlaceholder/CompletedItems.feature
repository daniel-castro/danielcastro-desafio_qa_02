@Test
Feature: Get completed items

  Scenario: Request Completed Items by GET
    Given url 'https://jsonplaceholder.typicode.com/todos'
    And param completed = 'true'
    When method get
    Then status 200