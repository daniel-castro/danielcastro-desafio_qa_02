@Test
Feature: Add Supplier
  User should be able to register a new Supplier through "ACCOUNTS >> SUPPLIERS" menu.
  User should be able to Search the new Supplier added through "ACCOUNTS >> SUPPLIERS" menu in order to validate its registration.

  Scenario: 1 - "ADD SUPPLIER” form allows the Admin user to register a new Supplier through "ACCOUNTS >> SUPPLIERS" menu
    Given the user is logged in to PHP Travels Admin Site
    When the user access SUPPLIERS MANAGEMENT page
    And clicks on ADD button
    And Submit a filled ADD SUPPLIER form
    Then the registered supplier can be seen on SUPPLIERS MANAGEMENT page