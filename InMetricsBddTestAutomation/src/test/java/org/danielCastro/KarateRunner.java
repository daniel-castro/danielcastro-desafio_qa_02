package org.danielCastro;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.junit4.Karate;
import org.junit.runner.RunWith;

@RunWith(Karate.class)
@KarateOptions(features = {
        "src/test/resources/WebService/FakeRestAPI/BooksPost.feature",
        "src/test/resources/WebService/JSONPlaceholder/CompletedItems.feature"})
public class KarateRunner {

}