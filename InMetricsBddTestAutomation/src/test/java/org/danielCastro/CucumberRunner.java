package org.danielCastro;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format={"pretty", "html:reports/test-report"}, tags = "@Test", features = {"src/test/resources/WebSite"})
public class CucumberRunner {


    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
    }


    @AfterClass
    public static void tearDownClass() throws InterruptedException {

        if (StepDefinition_PhpTravels.driver != null) {
            StepDefinition_PhpTravels.driver.quit();
        }
    }
}
