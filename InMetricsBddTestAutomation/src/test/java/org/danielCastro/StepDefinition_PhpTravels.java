package org.danielCastro;

import PageFactory.AddSupplierPage;
import PageFactory.HomePage;
import PageFactory.LoginPage;
import PageFactory.SuppliersMgmtPage;
import cucumber.api.java.en.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.UtilitiesBelt;
import java.lang.*;

public class StepDefinition_PhpTravels {

    public static WebDriver driver = new ChromeDriver();

    static WebDriverWait wait = new WebDriverWait(driver, 30);
    static UtilitiesBelt tool = new UtilitiesBelt();

    String email = "admin@phptravels.com";
    String password = "demoadmin";
    String sign_in_url = "https://www.phptravels.net/admin";

    String newSupplier_firstname = tool.firstNameGenerator();
    String newSupplier_lastname = tool.lastNameGenerator();
    String newSupplier_email = newSupplier_firstname + "@" + newSupplier_lastname + ".com";
    String newSupplier_password = tool.passwordGenerator(12);
    String newSupplier_mobileNumber = tool.phoneGenerator(9);;
    String newSupplier_address1 = tool.address1Generator();
    String newSupplier_address2 = tool.address2Generator();
    String newSupplier_itemName = newSupplier_lastname + "'s Inn";

    static LoginPage objLogin = new LoginPage(driver);
    static HomePage objHome;
    static SuppliersMgmtPage objSuppliersMgmt;
    static AddSupplierPage objAddSupplier;

    @Given("^the user is logged in to PHP Travels Admin Site$")
    public void the_user_is_logged_in_to_Php_Travels_Admin_Site(){
        objLogin.getLoginPage(driver, sign_in_url);
        objLogin.login(email, password);
        objHome = new HomePage(driver);
    }

    @When("^the user access SUPPLIERS MANAGEMENT page$")
    public void the_user_access_SUPPLIERS_MANAGEMENT_page(){
        objHome.accessSuppliersManagementPage();
        objSuppliersMgmt = new SuppliersMgmtPage(driver);
    }

    @And("^clicks on ADD button$")
    public void clicks_On_Add_Button(){
        objSuppliersMgmt.accessAddSupplierPage();
        objAddSupplier = new AddSupplierPage(driver);
    }

    @And("^Submit a filled ADD SUPPLIER form$")
    public void submitAFilledADDSUPPLIERForm() {
        objAddSupplier.AddingSupplier(
                newSupplier_firstname,
                newSupplier_lastname,
                newSupplier_email,
                newSupplier_password,
                newSupplier_mobileNumber,
                newSupplier_address1,
                newSupplier_address2,
                newSupplier_itemName
                );
    }

    @Then("^the registered supplier can be seen on SUPPLIERS MANAGEMENT page$")
    public void theRegisteredSupplierCanBeSeenOnSUPPLIERSMANAGEMENTPage() {
        objSuppliersMgmt.searchNewSupplier(newSupplier_email);
    }
}