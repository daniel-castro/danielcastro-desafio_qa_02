package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.UtilitiesBelt;

public class SuppliersMgmtPage {

    WebDriver driver;
    WebDriverWait wait;
    static UtilitiesBelt tool = new UtilitiesBelt();

    @FindBy(xpath="//form[contains(@class, \"add_button\")]/button[contains(text(), \" Add\")]")
    WebElement addButton;

    @FindBy(xpath="//div[contains(@class, \"xcrud-nav\")]/a[contains(text(), \"Search\")]")
    WebElement searchButton;

    @FindBy(xpath="//input[contains(@class, \"xcrud-searchdata xcrud-search-active input-small form-control\")][contains(@name, \"phrase\")]")
    WebElement searchField;

    @FindBy(xpath="//div[contains(@class, \"xcrud-nav\")]/span/span/a[contains(text(), \"Go\")]")
    WebElement goButton;

    public SuppliersMgmtPage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

        PageFactory.initElements(driver, this);
    }

    public void clickAddButton(){
        System.out.println("Clicking Add button on Suppliers Management page...");
        wait.until(ExpectedConditions.elementToBeClickable(addButton));
        addButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, \"panel-heading\")][contains(text(), \"Add Supplier\")]")));
    }

    public void clickSearchButton(){
        System.out.println("Clicking Search button on Suppliers Management page...");
        wait.until(ExpectedConditions.elementToBeClickable(searchButton));
        searchButton.click();
    }

    public void setSearchField(String email){
        System.out.println("Setting the password on login page...");
        wait.until(ExpectedConditions.elementToBeClickable(searchField));
        tool.clearSendKeysField(searchField, email);
    }

    public void accessAddSupplierPage(){
        this.clickAddButton();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class, \"panel-heading\")][contains(text(), \"Add Supplier\")]")));
    }

    public void searchNewSupplier(String email){
        System.out.println("Searching the new Supplier just registered...");
        this.clickSearchButton();
        this.setSearchField(email);
        goButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[contains(@class, \"xcrud-row xcrud-row-0\")]/td/a[contains(text(), \"" + email + "\")]")));
    }
}
