package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.UtilitiesBelt;

public class AddSupplierPage {

    WebDriver driver;
    WebDriverWait wait;
    static UtilitiesBelt tool = new UtilitiesBelt();

    @FindBy(name="fname")
    WebElement firstNameField;

    @FindBy(name="lname")
    WebElement lastNameField;

    @FindBy(name="email")
    WebElement emailField;

    @FindBy(name="password")
    WebElement passwordField;

    @FindBy(name="mobile")
    WebElement mobileNumberField;

    @FindBy(xpath="//a[contains(@class,\"select2-choice\")]")
    WebElement countrySelect;

    @FindBy(xpath="//div[contains(@class, \"select2-result-label\")][contains(text(), \"Brazil\")]")
    WebElement countrySelectOption_Brazil;

    @FindBy(name="address1")
    WebElement address1Field;

    @FindBy(name="address2")
    WebElement address2Field;

    @FindBy(name="status")
    WebElement statusSelect;

    @FindBy(xpath="//select[contains(@name, \"status\")]/option[contains(text(), \"Enabled\")]")
    WebElement statusSelectOption_Enabled;

    @FindBy(name="applyfor")
    WebElement supplierForSelect;

    @FindBy(xpath="//select[contains(@name, \"applyfor\")]/option[contains(@value, \"Hotels\")]")
    WebElement supplierForSelectOption_Hotels;

    @FindBy(name="itemname")
    WebElement itemNameField;

    @FindBy(xpath="//input[contains(@name, \"newssub\")]/following-sibling::ins/parent::div")
    WebElement newsletterCheckbox;

    @FindBy(xpath="//label[contains(text(), \"Assign Hotels\")]/following-sibling::div/descendant::input")
    WebElement assignHotelsSelect;

    @FindBy(xpath="//div[contains(@class, \"select2-result-label\")][contains(text(), \"Rendezvous Hotels\")]")
    WebElement assignHotelsSelectOption_RendezvousHotels;

    @FindBy(xpath="//label[contains(text(), \"Assign Tours\")]/following-sibling::div/descendant::input")
    WebElement assignToursSelect;

    @FindBy(xpath="//div[contains(@class, \"select2-result-label\")][contains(text(), \"Spectaculars Of The Nile 3 Nights\")]")
    WebElement assignToursSelectOption_SpectacularsOfTheNile;

    @FindBy(xpath="//label[contains(text(), \"Assign Cars\")]/following-sibling::div/descendant::input")
    WebElement assignCarsSelect;

    @FindBy(xpath="//div[contains(@class, \"select2-result-label\")][contains(text(), \"car1\")]")
    WebElement assignCarsSelectOption_Car1;

    @FindBy(xpath="//input[contains(@value, \"addHotels\")]/following-sibling::ins/parent::div")
    WebElement permissionsAddHotels;

    @FindBy(xpath="//input[contains(@value, \"editTours\")]/following-sibling::ins/parent::div")
    WebElement permissionsEditTours;

    @FindBy(xpath="//input[contains(@value, \"deleteCars\")]/following-sibling::ins/parent::div")
    WebElement permissionsDeleteCars;

    @FindBy(xpath="//div[contains(@class, \"panel-footer\")]/button[contains(text(), \"Submit\")]")
    WebElement submitButton;

    @FindBy(xpath="//h4[contains(@class, \"ui-pnotify-title\")][contains(text(), \"Changes Saved!\")]")
    WebElement notificationChangesSaved;

    public AddSupplierPage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

        PageFactory.initElements(driver, this);
    }

    public void setFirstNameField(String firstname){
        System.out.println("Setting the First Name field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(firstNameField));
        tool.clearSendKeysField(firstNameField, firstname);
    }

    public void setLastNameField(String lastname){
        System.out.println("Setting the Last Name field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(firstNameField));
        tool.clearSendKeysField(lastNameField, lastname);
    }

    public void setEmailField(String email){
        System.out.println("Setting the E-mail field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(emailField));
        tool.clearSendKeysField(emailField, email);
    }

    public void setPasswordField(String password){
        System.out.println("Setting the Password field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(passwordField));
        tool.clearSendKeysField(passwordField, password);
    }

    public void setMobileNumberField(String mobileNumber){
        System.out.println("Setting the Mobile Number field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(mobileNumberField));
        tool.clearSendKeysField(mobileNumberField, mobileNumber);
    }

    public void selectCountry(){
        System.out.println("Selecting the Country on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(countrySelect));
        countrySelect.click();
        wait.until(ExpectedConditions.elementToBeClickable(countrySelectOption_Brazil));
        countrySelectOption_Brazil.click();
    }

    public void setAddress1Field(String address1){
        System.out.println("Setting the Address 1 field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(address1Field));
        tool.clearSendKeysField(address1Field, address1);
    }

    public void setAddress2Field(String address2){
        System.out.println("Setting the Address 2 field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(address2Field));
        tool.clearSendKeysField(address2Field, address2);
    }

    public void selectStatus(){
        System.out.println("Selecting the Status on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(statusSelect));
        statusSelect.click();
        wait.until(ExpectedConditions.elementToBeClickable(statusSelectOption_Enabled));
        statusSelectOption_Enabled.click();
    }

    public void selectSupplierFor(){
        System.out.println("Selecting the Supplier For option on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(supplierForSelect));
        supplierForSelect.click();
        wait.until(ExpectedConditions.elementToBeClickable(supplierForSelectOption_Hotels));
        supplierForSelectOption_Hotels.click();
    }

    public void setItemNameField(String itemName){
        System.out.println("Setting the Item Name field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(itemNameField));
        tool.clearSendKeysField(itemNameField, itemName);
    }

    public void checkNewsletterCheckbox(){
        System.out.println("Setting the Item Name field on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(newsletterCheckbox));
        newsletterCheckbox.click();
    }

    public void setAssignHotelsSelect(){
        System.out.println("Selecting the Assign Hotels option on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(assignHotelsSelect));
        assignHotelsSelect.click();
        wait.until(ExpectedConditions.elementToBeClickable(assignHotelsSelectOption_RendezvousHotels));
        assignHotelsSelectOption_RendezvousHotels.click();
    }

    public void setAssignToursSelect(){
        System.out.println("Selecting the Assign Tours option on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(assignToursSelect));
        assignToursSelect.click();
        wait.until(ExpectedConditions.elementToBeClickable(assignToursSelectOption_SpectacularsOfTheNile));
        assignToursSelectOption_SpectacularsOfTheNile.click();
    }

    public void setAssignCarsSelect(){
        System.out.println("Selecting the Assign Cars option on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(assignCarsSelect));
        assignCarsSelect.click();
        wait.until(ExpectedConditions.elementToBeClickable(assignCarsSelectOption_Car1));
        assignCarsSelectOption_Car1.click();
    }

    public void checkPermissionsAddHotels(){
        System.out.println("Setting the Add permission on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(permissionsAddHotels));
        permissionsAddHotels.click();
    }

    public void checkPermissionsEditTours(){
        System.out.println("Setting the Edit permission on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(permissionsEditTours));
        permissionsEditTours.click();
    }

    public void checkPermissionsDeleteCars(){
        System.out.println("Setting the Delete permission on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(permissionsDeleteCars));
        permissionsDeleteCars.click();
    }

    public void clickSubmitButton() {
        System.out.println("Clicking Submit button on Add Supplier page...");
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
        submitButton.click();
    }

    public void AddingSupplier(String firstname,
                               String lastname,
                               String email,
                               String password,
                               String mobileNumber,
                               String address1,
                               String address2,
                               String itemName){
        this.setFirstNameField(firstname);
        this.setLastNameField(lastname);
        this.setEmailField(email);
        this.setPasswordField(password);
        this.setMobileNumberField(mobileNumber);
        this.selectCountry();
        this.setAddress1Field(address1);
        this.setAddress2Field(address2);
        this.selectStatus();
        this.selectSupplierFor();
        this.setItemNameField(itemName);
        this.checkNewsletterCheckbox();
        this.setAssignHotelsSelect();
        this.setAssignToursSelect();
        this.setAssignCarsSelect();
        this.checkPermissionsAddHotels();
        this.checkPermissionsEditTours();
        this.checkPermissionsDeleteCars();
        this.clickSubmitButton();
        wait.until(ExpectedConditions.elementToBeClickable(notificationChangesSaved));
    }
}
