package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.UtilitiesBelt;

public class LoginPage {

    WebDriver driver;
    WebDriverWait wait;
    static UtilitiesBelt tool = new UtilitiesBelt();

    @FindBy(xpath="//h2[contains(text(), \"Login Panel\")]")
    WebElement loginPanelHeading;

    @FindBy(xpath="//input[contains(@name, \"email\")]")
    WebElement emailField;

    @FindBy(xpath="//input[contains(@name, \"password\")]")
    WebElement passwordField;

    @FindBy(xpath="//button/span[contains(@class, \"ladda-label\")][contains(text(), \"Login\")]")
    WebElement loginButton;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);

        PageFactory.initElements(driver, this);
    }

    public void getLoginPage(WebDriver driver, String url){
        System.out.println("Loading login page...");
        driver.get(url);
        wait.until(ExpectedConditions.elementToBeClickable(loginPanelHeading));
    }

    public void setEmail(String email){
        System.out.println("Setting the email as username on login page...");
        wait.until(ExpectedConditions.elementToBeClickable(emailField));
        tool.clearSendKeysField(emailField, email);
    }

    public void setPassword(String password){
        System.out.println("Setting the password on login page...");
        wait.until(ExpectedConditions.elementToBeClickable(passwordField));
        tool.clearSendKeysField(passwordField, password);
    }

    public void clickLoginButton(){
        System.out.println("Clicking login button on login page...");
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        loginButton.click();
    }

    public void login(String email, String password){
        this.setEmail(email);
        this.setPassword(password);
        this.clickLoginButton();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@href, \"https://www.phptravels.net/admin/logout\")]")));
    }
}
