$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("AddSupplier.feature");
formatter.feature({
  "line": 2,
  "name": "Add Supplier",
  "description": "User should be able to register a new Supplier through \"ACCOUNTS \u003e\u003e SUPPLIERS\" menu.\r\nUser should be able to Search the new Supplier added through \"ACCOUNTS \u003e\u003e SUPPLIERS\" menu in order to validate its registration.",
  "id": "add-supplier",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Test"
    }
  ]
});
formatter.scenario({
  "line": 6,
  "name": "1 - \"ADD SUPPLIER” form allows the Admin user to register a new Supplier through \"ACCOUNTS \u003e\u003e SUPPLIERS\" menu",
  "description": "",
  "id": "add-supplier;1---\"add-supplier”-form-allows-the-admin-user-to-register-a-new-supplier-through-\"accounts-\u003e\u003e-suppliers\"-menu",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 7,
  "name": "the user is logged in to PHP Travels Admin Site",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "the user access SUPPLIERS MANAGEMENT page",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "clicks on ADD button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Submit a filled ADD SUPPLIER form",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "the registered supplier can be seen on SUPPLIERS MANAGEMENT page",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinition_PhpTravels.the_user_is_logged_in_to_Php_Travels_Admin_Site()"
});
formatter.result({
  "duration": 34616621689,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_PhpTravels.the_user_access_SUPPLIERS_MANAGEMENT_page()"
});
formatter.result({
  "duration": 4329585343,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_PhpTravels.clicks_On_Add_Button()"
});
formatter.result({
  "duration": 4193093105,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_PhpTravels.submitAFilledADDSUPPLIERForm()"
});
formatter.result({
  "duration": 14757729190,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition_PhpTravels.theRegisteredSupplierCanBeSeenOnSUPPLIERSMANAGEMENTPage()"
});
formatter.result({
  "duration": 1292651291,
  "status": "passed"
});
});