![Alt text](inmetrics.png)

# **Desafio QA - Daniel Castro**

---


## Tecnologias

* *Para documentar os cenários especificados seguindo BDD*: **Cucumber**
* *Linguagem de programação*: **Java**
* *IDE*: **IntelliJ Community**
* *Bibliotecas*:
>* 		**Selenium**
>*		**JUnit**
>*		**Cucumber**
>*		**Karate**

---

## Projeto
Projeto Maven: **InMetricsBddTestAutomation**

---

## Setup do Ambiente para execução dos testes

Para executar os cenários de teste automatizados, é necessário configurar o seu ambiente com os seguintes recursos:

* **JRE**
* **Maven**

---

## Execução dos Testes

Após realizar o setup informado acima, execute os cenários de teste automatizados através do seguinte procedimento:

* *Faça download do projeto*: **https://bitbucket.org/daniel-castro/danielcastro-desafio_qa_02/downloads/**
* *Descompacte o arquivo compactado em um diretório de sua preferência*
* *Acesse o promp de comando*: **Win+R -> CMD -> Enter**
* *Acesse o diretório para onde o arquivo compactado foi descompactado*: **cd <diretório>/InMetricsBddTestAutomation -> Enter**
* *Execute o teste do Desafio WebSite*: **mvn test -Dtest=CucumberRunner**
* *Verifique o resultado do teste do Desafio WebSite*: 
>* **<diretório>\reports\test-report\index.html**
* *Execute o teste do Desafio WebService*: **mvn test -Dtest=KarateRunner**
* *Verifique o resultado do teste do Desafio WebService*: 
>* **<diretório>\target\surefire-reports\src.test.resources.WebService.FakeRestAPI.BooksPost.html**
>* **<diretório>\target\surefire-reports\src.test.resources.WebService.JSONPlaceholder.CompletedItems.html**

---